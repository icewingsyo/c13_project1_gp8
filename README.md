- **sudo service**
```
sudo service postgresql start
sudo su postgres
sudo -U <username> -W -h <hostname> <DBname>
```
- **Main add(yarn)**
```
sudo npm install -g yarn
```
```
yarn add --dev ts-node typescript @types/node
yarn add --dev jest
yarn add --dev typescript ts-jest @types/jest
yarn ts-jest config:init
```
- [ ]BAD003
```
yarn add knex @types/knex pg @types/pg
```
- [ ]knexfile.ts
```
yarn knex init -x ts
```
- environment change (default development)
```
knex --env production <any-command>
```