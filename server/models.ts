export interface User {
    id: number;
    username: string;
    password: string;
    email: string;
    phone_number: number;
    role: string;
    created_at: Date;
    updated_at: Date;
}

export interface Post {
    
    address: string;
    date: Date;
    starting_time: TimeRanges;
    ending_time: TimeRanges;
    contact_number: number;
    number_of_student: number;
    price: string;
    description: Text;
    created_at: Date;
    updated_at: Date;
    created_by: string;
}

export interface Book{
    id:number;
    post_id:number;
    student_id:number;
    created_at: Date;
    updated_at: Date;
}
