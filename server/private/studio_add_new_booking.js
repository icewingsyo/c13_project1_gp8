window.onload = () => {
    loadAndPost();
};

function loadAndPost() {

    const postForm = document.getElementById("fill-in");

    postForm.addEventListener('submit', async function (event) {

        event.preventDefault();
        const formObject = {};
        formObject["address"] = postForm.address.value;
        formObject["date"] = postForm.date.value;
        formObject["starting_time"] = postForm.starting_time.value;
        formObject["ending_time"] = postForm.ending_time.value;
        formObject["contact_number"] = postForm.contact_number.value;
        formObject["number_of_student"] = postForm.number_of_student.value;
        formObject["price"] = postForm.price.value;
        formObject["description"] = postForm.description.value;
        
 
        const res = await fetch("/studio_detail", {
            method: "post",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(formObject),
        });
        if (res.status === 200) {
            window.location = "/studio_detail.html";
        } else {
            const data = await res.json();
            alert(data.message);
        }
    });
}