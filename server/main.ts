import express from 'express';
import expressSession from 'express-session';
import path from 'path';
import dotenv from "dotenv";
import pg from "pg";
import {isLoggedIn} from "./guards";
import Knex from 'knex';
import { BookService } from './routers/services/BookService';
import { UserService } from './routers/services/UserService';
import { PostService } from './routers/services/PostService';
import { BookController } from './routers/controllers/BookController';
import { UserController } from './routers/controllers/UserController';
import { PostController } from './routers/controllers/PostController';

dotenv.config()

const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

export const client = new pg.Client({
    user: process.env.DB_USERNAME,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
});

client.connect();

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const sessionMiddleware = expressSession({
    secret: "Tecky Academy teaches typescript",
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false },
});

app.use(sessionMiddleware);

app.use((req, res, next) => {
    console.log(`[info] method: ${req.method} path: ${req.url}`);
    next();
});

app.use(express.static(path.join(__dirname,"public")));

const bookService = new BookService(knex);
const userService = new UserService(knex);
const postService = new PostService(knex);
export const bookController = new BookController(bookService);
export const userController = new UserController(userService);
export const postController = new PostController(postService);

import {userRoutes} from "./routers/userRoutes"
import {bookRoutes} from "./routers/bookRoutes"
import {postRoutes} from "./routers/postRoutes"
app.use(userRoutes);
app.use(bookRoutes);
app.use(postRoutes);


app.use(isLoggedIn, express.static(path.join(__dirname, "private")));

const PORT = 8080;
app.listen(PORT, () => {
    console.log(`connected to localhost:${PORT}`)
})
