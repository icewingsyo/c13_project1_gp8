import { Book } from '../../models';
import { QueryResult } from 'pg';
import * as Knex from 'knex';

export class BookService {
    constructor(private knex:Knex){}

     async getStudentByID(id:number){
         return (await this.knex.select('id').from('student').where("user_id", id))[0].id;//[0].id;
     }//get student id (student)
    async getStudentByIDReturnID(id:number){
        return (await this.knex.select('id').from('student').where("user_id", id))[0].id;//.rows[0].id?
    }//get student id (student)
    async getCurrPostNotInBooking(id:number):Promise<Book[]>{
        return (await this.knex.select(this.knex.raw('now()'),'*').from('post')
        .whereNotIn('id', this.knex.select('post_id').from('booking').where('student_id', id)).orderBy('date', 'asc'));
    }//get all booking not in post (student)
    async createBook(id:number, postID:number):Promise<QueryResult<Book>>{
        return (await this.knex('booking').insert({
            post_id: `${postID}`,
            student_id: `${id}`
        }));
    }//create Booking (student)
    async bookedStudent(postID:number):Promise<Book[]>{
        return (await this.knex.select('users.id', 'users.username', 'users.email', 'users.phone_number').from('booking')
               .innerJoin('student', 'booking.student_id', 'student.id')
               .innerJoin('users', 'student.user_id', 'users.id')
               .where('booking.post_id', postID));//.rows?
    }// check booked student (studio)
    async getCurrPostInBooking(id:number):Promise<Book[]>{
        return (await this.knex.select(this.knex.raw('now()'),'*').from('post')
        .whereIn('id', this.knex.select('post_id').from('booking').where('student_id', id)).orderBy('date', 'asc'));
        
    }//get all booking in post (student)
    async cancelBooking(id:number, postID:number){
        return (await this.knex('booking').where('post_id', postID).andWhere('student_id', id).del());
    }// delete booking (student)
}