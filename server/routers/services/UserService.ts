import {User} from '../../models';
//import { Client } from 'pg';
import * as Knex from 'knex';

export class UserService{
    constructor(private knex:Knex){}

    async getUserEmail(email:string){
        return (await this.knex('users').count('*').where('email', email));
    }// get user email (check duplicated user)
    async getUserToLogin(email:string){
        return (await this.knex.select('*').from('users').where('email', email))[0];
    }// get user email to login
    async getStudent(id:number):Promise<User>{
        return (await this.knex.select('*').from('users').where('id', id))[0];
    }
    async createUser(
        name:string,
        email:string,
        password:string,
        phone_number:string,
        role:string
    ){
        return (await this.knex.insert({
            username: `${name}`,
            email: `${email}`,
            password: `${password}`,
            phone_number: `${phone_number}`,
            role: `${role}`
        }).into('users').returning('id'))[0];
        
    }//create users
        /*this.client.query(`
        INSERT INTO users (username, email, password, phone_number, role) 
        values ($1, $2, $3, $4, $5) returning id
        `,[name, email, password, phone_number, role])).rows[0].id;*/
    
    async createStudio(id:any){
        return (await this.knex('studio').insert({
            user_id: `${id}`
        }));
    }//after createUser() studio
    
    async createStudent(id:any){
        return (await this.knex('student').insert({
            user_id: `${id}`
        }));
    }//after createUser() student
}