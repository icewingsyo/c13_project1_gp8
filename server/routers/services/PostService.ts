import {Post} from '../../models';
//import { Client } from 'pg';
import * as Knex from 'knex';

export class PostService{
    constructor(private knex:Knex){}

    async getStudioByID(id:number){
        /*return (await this.client.query(`
        SELECT id FROM studio where user_id = $1
        `,[id])).rows[0].id;*/
        return (await this.knex.select('id').from('studio').where('user_id', id))[0].id;
    }// get studio id from studio table
    async createPost(
        address:string,
        date:Date,
        starting_time:Date,
        ending_time:Date,
        contact_number:string,
        number_of_student:number,
        price:number,
        description:string,
        created_by:number
    )
    {
        /*return (await this.client.query(`
        INSERT INTO post (address, date, starting_time, ending_time, contact_number, number_of_student, price, description, created_by) 
                    values ($1, $2, $3, $4, $5, $6, $7, $8, $9)
          `,[address, date, starting_time, ending_time, contact_number, number_of_student, price, description, created_by]
        ));*/
        return (await this.knex('post').insert({
            address: `${address}`, date: `${date}`, starting_time: `${starting_time}`,
            ending_time: `${ending_time}`, contact_number: `${contact_number}`, 
            number_of_student: `${number_of_student}`, price: `${price}`, 
            description: `${description}`, created_by: `${created_by}`
        }));
    }// create post

    async getPost(studioID:number):Promise<Post[]>{
        /*return (await this.client.query(`
        SELECT * FROM post where created_by = $1 ORDER BY id desc;
        `,[id])).rows;*/
        return (await this.knex.select('*').from('post').where('created_by', studioID).orderBy('id', 'asc')); //.rows
    }// get post by created_by (studio)
    async getPostById(studioID:number, post_id:number):Promise<Post[]>{
        /*return (await this.client.query(`
        SELECT * FROM post where created_by = $1 and id = $2 ORDER BY id desc;
        `,[id,post_id])).rows[0];*/
        return (await this.knex.select('*').from('post').where('created_by', studioID).andWhere('id', post_id).orderBy('id', 'desc'))[0]; //.rows[0]
    }// get post by login studio (studio)
    async getUserByStudio(id:number){
        /*return (await this.client.query(`
        SELECT username, email, phone_number, role FROM users where id = $1
        `, [id])).rows[0];*/
        return (await this.knex.select('username', 'email', 'phone_number', 'role').from('users').where('id', id))[0]; //rows[0]
    }// get login studio info (greeting)
    async updatePost(
        id:number,
        post_id:number,
        address:string,
        date:Date,
        starting_time:Date,
        ending_time:Date,
        contact_number:string,
        number_of_student:number,
        price:number,
        description:string,
        ):Promise<Post[]>{
            /*return (await this.client.query(`
            UPDATE post SET address = $1, date = $2, starting_time = $3, 
            ending_time = $4, contact_number = $5, number_of_student = $6, 
            price = $7, description = $8 ,created_by = $9 WHERE id = $10
            `,
            [ address, date, starting_time, ending_time, contact_number, number_of_student, price, description, id, post_id]
        )).rows;*/
        return (await this.knex('post').where('id', post_id)
        .update({
            address: `${address}`, date: `${date}`, starting_time: `${starting_time}`, ending_time: `${ending_time}`, 
            contact_number: `${contact_number}`, number_of_student: `${number_of_student}`, 
            price: `${price}`, description: `${description}`, created_by: `${id}`
        })
        ); //rows
    }// update table post by login studio
}