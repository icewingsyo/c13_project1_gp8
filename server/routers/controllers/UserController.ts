import { Request, Response} from 'express';
import {UserService} from '../services/UserService';
import { checkPassword, hashPassword } from "../../hash";

export class UserController{
    constructor(private userService:UserService){}

    createUser = async (req:Request, res:Response)=>{
        try{
            const { name, email, password, phone_number, role } = req.body;
            console.log(role);
            const userCount = await this.userService.getUserEmail(email);
            if(userCount[0]["count"] != 0){
                res.status(400).json({ message: "duplicated user" });
                return;
            };
            const hashedPassword = await hashPassword(password);
            const newID = await this.userService.createUser(name, email, hashedPassword, phone_number, role);
            if(role == 'studio'){this.userService.createStudio(newID);}
            else {this.userService.createStudent(newID);}
            res.redirect("/index.html") // <-use in .js file
            // res.status(200).json({})
        }catch(err){
            console.log(err);
            res.status(500).json({ message: "Server Error!" });
        }
    }
    login = async (req:Request, res:Response)=>{
        try{
            const { email, password } = req.body;
            const userResult = await this.userService.getUserToLogin(email);
            if(!userResult){
                res.status(400).json({ message: "invalid username" });
                return;
            };
            const matchPassword = await checkPassword(password,userResult.password );
            if(!matchPassword){
                res.status(400).json({ message: "invalid password" });
                return;
            };
            req.session["user"] = {email: userResult.email, role: userResult.role, id: userResult.id};
            // let location = "/studio_detail.html"
            // if (user.role !== "studio") {
            //     location = "/student_detail.html"
            // };
            res.json({message: "success", role: userResult.role});
            //get role to .js to set the location
        }catch(err){
            console.error(err.message);
            res.status(500).json({ message: "internal server error" });
        }
    }
    logout = async (req:Request, res:Response)=>{
        if(req.session){
            delete req.session["user"];
            res.redirect('/index.html');//handle in .js
        }
    }
    getStudent = async (req:Request, res:Response)=>{
        const userID:number = req.session["user"].id;
        const userResult = await this.userService.getStudent(userID);
        res.json({data: userResult, message: "user id got it"});
    }
}
