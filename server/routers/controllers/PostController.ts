import { Request, Response } from "express";
import { PostService } from '../services/PostService';

export class PostController {
    constructor(private postService: PostService) { }

    createPost = async (req: Request, res: Response) => {
        try {
            const userID = parseInt(req.session["user"].id);
            const studioID = await this.postService.getStudioByID(userID);
            const { address, date, starting_time, ending_time, contact_number, number_of_student, price, description } = req.body;
            await this.postService.createPost(
                address, date, starting_time, ending_time, contact_number, parseInt(number_of_student), parseInt(price), description, studioID
            );
            res.json({ message: "Post is posted" });
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Server Error" });
        }
    }
    getPost = async (req: Request, res: Response) => {
        try {
            const userID = parseInt(req.session["user"].id);
            const studioID = await this.postService.getStudioByID(userID);
            const postResult = await this.postService.getPost(studioID);
            res.json({ data: postResult, message: "posts get it" });
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Server Error" });
        }
    }
    getPostById = async (req: Request, res: Response) => {
        try {
            const userID = parseInt(req.session["user"].id);
            const postID = parseInt(req.params.id);
            const studioID = await this.postService.getStudioByID(userID);
            const postResult = await this.postService.getPostById(studioID, postID);
            res.json(postResult);
            res.json({message: "post get by studio success" });
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "Error" });
        }
    }
    getUserByStudio = async (req:Request, res:Response)=>{
        try{
            const userID = parseInt(req.session["user"].id);
            const userResult = await this.postService.getUserByStudio(userID);
            res.json({data: userResult, message:"users got it"});
        }catch(err){
            console.log(err.message);
            res.status(500).json({ message: "Error" });
        }
    }
    updatePost = async (req:Request, res:Response)=>{
        try{
        const userID = parseInt(req.session["user"].id);
        const postID = parseInt(req.params.id);
        const { address, date, starting_time, ending_time, contact_number, number_of_student, price, description} = req.body;
        const studioID = await this.postService.updatePost(
            userID, postID, address, date, starting_time, ending_time, contact_number, parseInt(number_of_student), parseInt(price), description
        );
        res.json({data: studioID, message: "post is updated"});
        }catch(err){
            console.log(err.message);
            res.status(500).json({ message: "Error" });
        }
    }
}