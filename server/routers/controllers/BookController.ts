import { Request, Response } from 'express';
import {BookService} from '../services/BookService';


export class BookController{
    constructor(private bookService:BookService){}

    getCurrPostNotInBooking = async (req:Request, res:Response)=>{
        try{
            const userID:number = parseInt(req.session["user"].id);
            let studentID:number = await this.bookService.getStudentByID(userID);
            //console.log(studentID);
            let bookResult = await this.bookService.getCurrPostNotInBooking(studentID);
            //console.log(bookResult);
            res.json({data: bookResult, message: "Your books got it" });
            //console.log('not in last user id:',userID);
        }catch(err){
            console.log(err.message);
            res.status(500).json({ message: "Error"});
        }
    }
    createBook = async (req:Request, res:Response)=>{
        try{
            const userID:number = parseInt(req.session["user"].id);
            const postID:number = parseInt(req.params.post_id);
            let studentID:number = await this.bookService.getStudentByID(userID);
            await this.bookService.createBook(studentID, postID);
            res.json({ message: "Booking is created"});
        }catch(err){
            console.log(err.message);
            res.status(500).json({ message: "error" });
        }
    }
    bookedStudent = async(req:Request, res:Response)=>{
        try{
            const postID:number = parseInt(req.params.id);
            let bookedStudent = await this.bookService.bookedStudent(postID);
            res.json({data: bookedStudent, message: "success get student booked"});
        }catch(err){
            console.log(err.message);
        res.status(500).json({ message: "error" });
        }
    }
    getCurrPostInBooking = async (req:Request, res:Response)=>{
        try{
            const userID:number = parseInt(req.session["user"].id);
            let studentID:number = await this.bookService.getStudentByID(userID);
            let bookResult = await this.bookService.getCurrPostInBooking(studentID);
            //console.log(bookResult);
            res.json({data: bookResult, message: "Your books got it" });
            //console.log('in last user id:',userID);
        }catch(err){
            console.log(err.message);
            res.status(500).json({ message: "Error"});
        }
    }
    cancelBooking = async(req:Request, res:Response)=>{
        try{
            const userID:number = parseInt(req.session["user"].id);
            const postID:number = parseInt(req.params.id);
            const studentID = await this.bookService.getStudentByIDReturnID(userID);
            await this.bookService.cancelBooking(studentID, postID);
            res.json({message: "Booking Canceled."});
        }catch(err){
            console.log(err.message);
            res.status(500).json({ message: "error" });
        }
    }
}