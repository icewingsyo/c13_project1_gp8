import express from "express";
import { bookController } from '../main';

export const bookRoutes = express.Router();

bookRoutes.get("/currectBooks", bookController.getCurrPostNotInBooking);
// bookRoutes.get("/currectStudent", getStudent); //use userControllers
// bookRoutes.post("/currectStudent");
bookRoutes.post("/book/:post_id", bookController.createBook);
bookRoutes.get("/post/book/:id", bookController.bookedStudent);
// bookRoutes.post("/currectStudent");
bookRoutes.post("/bookingCourse/:post_id", bookController.createBook);
bookRoutes.get("/studentBookingValue", bookController.getCurrPostInBooking);
bookRoutes.post("/cancelBooking/:id", bookController.cancelBooking);