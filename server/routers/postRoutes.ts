import express from "express";
import { postController } from "../main";


export const postRoutes = express.Router();

postRoutes.post("/studio_detail", postController.createPost);
postRoutes.get("/posts", postController.getPost);
postRoutes.get("/post/:id", postController.getPostById);
postRoutes.put("/post/:id", postController.updatePost);
postRoutes.get("/users", postController.getUserByStudio);
