window.onload = () => {
    initLoginForm();
};

function initLoginForm() {
    
    const loginForm = document.getElementById("login-form");//get login form at index.html
    
    loginForm.addEventListener('submit', async function (event) {
        
        
        event.preventDefault();
        const formObject = {};
        formObject["email"] = loginForm.email.value; // Server: req.body.username
        formObject["password"] = loginForm.password.value; // Server: req.body.password
        console.log(formObject)
        const res = await fetch("/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(formObject),
        });
        
        if (res.status === 200) {
            const data = await res.json();
            let location = "/studio_detail.html"
            if (data.role !== "studio") {
                location = "/student_detail.html"
             };
            window.location = location;
        } else {
            const data = await res.json();
            alert(data.message);
        }
    });
}

